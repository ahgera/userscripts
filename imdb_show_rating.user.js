// ==UserScript==
// @name        IMdB Show rating
// @namespace   nope
// @description Fetch ratings for movies in filmography
// @include     http://www.imdb.com/name/*
// @include     https://www.imdb.com/name/*
// @version     6.1
// @grant       none
// ==/UserScript==

let filmography = document.querySelector('div#filmography');
if (!filmography) return;

const CACHE_TTL = 7 * 24 * 3600 * 1000; // one week
let ratingsDb = JSON.parse(localStorage.getItem('imdbCache')) || {};
setTimeout(() => { clearOldCache(ratingsDb); }, 3000);
let ratings = new Proxy(ratingsDb, {
    get: async function (cache, id) {
        if (id in cache && cache[id].timeStamp > Date.now() - CACHE_TTL) return cache[id];
        let rating = await fetchRating(id);
        if (rating.ratingValue == null) return rating;
        cache[id] = rating;
        localStorage.setItem('imdbCache', JSON.stringify(cache));
        return rating;
    }
});

let lazyLinkObserver = new IntersectionObserver(videos => {
    for (let video of videos) {
        if (video.isIntersecting) {
            addRating(video.target);
            lazyLinkObserver.unobserve(video.target);
        }
    }
}, { rootMargin: "100px" }); //start loading before it is visible

let films = filmography.querySelectorAll('div.filmo-row > b > a');
films.forEach(v => lazyLinkObserver.observe(v));

async function addRating(videoLink) {
    let id = videoLink.href.match(/tt[\d]{7}/);
    if (!id) return;

    let { ratingValue, userRating } = await ratings[id];
    if (ratingValue == null) return;
    let ratingNode = createRatingNode(ratingValue, userRating);
    if (ratingNode == null) return;
    videoLink.parentNode.insertBefore(ratingNode, videoLink);
}

async function fetchRating(id) {
    let url = 'https://www.imdb.com/title/' + id + '/ratings';
    let r = await fetch(url, {
        credentials: 'include',
        headers: {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
        }
    });
    let text = await r.text();
    let parser = new DOMParser();
    let doc = parser.parseFromString(text, 'text/html');
    let ratingElement = doc.querySelector('span.ipl-rating-star__rating');
    if (ratingElement == null)
        return { ratingValue: null, userRating: null, timeStamp: Date.now() };
    let ratingValue = +ratingElement.textContent;
    let userRatingText = doc.querySelector('div.ipl-rating-selector').dataset.value;
    let userRating = userRatingText === '0' ? null : +userRatingText;

    return { ratingValue: ratingValue, userRating: userRating, timeStamp: Date.now() };
}

function createRatingNode(rating, userRating) {
    if (rating.length < 1) return;

    let grades = [
        { val: 0.0, color: '#b01c1c' },
        { val: 5.5, color: '#a78719' },
        { val: 7.0, color: '#318b22' },
        { val: 10.0, color: '#28b521' }
    ];

    let ratingEl = document.createElement('span');
    let color = '';
    for (let i = 1; i < grades.length; i++) {
        if (rating < grades[i].val) {
            color = interpolateColor(grades[i - 1].color,
                grades[i].color,
                (rating - grades[i - 1].val) / (grades[i].val - grades[i - 1].val));
            break;
        }
    }
    ratingEl.textContent = parseFloat(rating).toFixed(1);
    ratingEl.style.color = color;
    ratingEl.style.marginRight = '4px';
    ratingEl.className = 'gm-rating';

    let wrapEl = document.createElement('span');
    wrapEl.style = 'margin-right: 5px;';
    wrapEl.className = 'gm-rating-wrap';
    wrapEl.appendChild(ratingEl);

    if (userRating) {
        let userRatingEl = document.createElement('span');
        userRatingEl.className = 'gm-rating-user';
        userRatingEl.textContent = userRating;
        userRatingEl.style.color = '#648eed';
        userRatingEl.style.marginLeft = '4px';

        wrapEl.appendChild(document.createTextNode('|'));
        wrapEl.appendChild(userRatingEl);
    }

    return wrapEl;
}

function interpolateColor(low, high, t) {
    // 0 <= t <= 1
    let c1 = hexToRgb(low);
    let c2 = hexToRgb(high);

    let result = rgbToHex(lerp(c1.r, c2.r, t),
        lerp(c1.g, c2.g, t),
        lerp(c1.b, c2.b, t));
    return result;
}

function lerp(v1, v2, t) {
    return parseInt(v1 + (v2 - v1) * t);
}

function hexToRgb(hex) {
    let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}
function componentToHex(c) {
    let hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}
function rgbToHex(r, g, b) {
    return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

function clearOldCache(db) {
    const now = Date.now();
    for (let item of Object.keys(db)) {
        let age = now - db[item].timeStamp;
        if (age > CACHE_TTL) {
            delete db[item];
        }
    }
    localStorage.setItem('imdbCache', JSON.stringify(db));
}
