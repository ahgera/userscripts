// ==UserScript==
// @name         Corren Show description
// @version      0.1
// @description  Shows the article description when hovering over a link
// @match        https://corren.se/nyheter
// @icon         https://www.google.com/s2/favicons?domain=corren.se
// @grant        none
// ==/UserScript==

function getDescription(hl) {
    let a = hl.closest("a");
    a.addEventListener("mouseover", (ev) => {
        if (!a.hasAttribute('data-fetched')) {
            let url = a.href;
            url = url.replace(/www.affarsliv.com/, "corren.se/nyheter");
            fetch(url)
                .then((r) => { return r.text(); })
                .then((html) => {
                let parser = new DOMParser();
                let doc = parser.parseFromString(html, "text/html");
                let desc = doc.querySelector('meta[name=description]').getAttribute("content");

                a.title = desc;
                a.dataset.fetched = true;
            });
        }
    });
}

function handleHeadlines() {
    let headlines = document.querySelectorAll('h1[class*="headline"]');
    for (let hl of headlines) {
        getDescription(hl);
    }
}

setTimeout(function(){ handleHeadlines(); }, 1000);
