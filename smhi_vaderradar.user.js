// ==UserScript==
// @name         SMHI Väderradar
// @version      0.9
// @description  Större karta, autouppdaterande. Minikarta som favicon
// @match        https://www.smhi.se/vader/radar-och-satellit/radar-blixt/*
// @grant        none
// ==/UserScript==



setTimeout(fullscreenMap, 100);
updateFavicon();

setInterval(() => { updateData(); updateFavicon(); }, 5*60*1000);

function fullscreenMap()
{
    let map_widget = document.querySelector('.leaflet-container').parentNode;
    map_widget.style = `position: fixed !important;
	top: 0 !important;
	left: 0 !important;
	right: 0 !important;
	bottom: 0 !important;
	width: 100% !important;
	height: 100% !important;
	margin: 0 !important;
	min-width: 0 !important;
	max-width: none !important;
	min-height: 0 !important;
	max-height: none !important;
	box-sizing: border-box !important;
	object-fit: contain;
	transform: none !important;`;

    document.getElementsByTagName('body')[0].style.overflow = 'hidden';

    // Center map in viewport
    window.dispatchEvent(new Event('resize'));

    // Remove cluttering elements
    ['#footer', '#extContent', '.global-top-header', '.kundo-forum-bottom'].forEach(s => {
        try {
            document.querySelector(s).remove()
        }
        catch (er) {}
    });

    setTimeout(fixFullscreenControls, 500);
}

// Update map controls after fullscreen
function fixFullscreenControls() {
    // Center on current location
    document.querySelector('[aria-label="Min position"]').click();

    // Put slider at the bottom
    document.querySelector('[aria-label="Starta uppspelning"]').parentNode.style = `width: 90%;
	position: fixed;
	top: calc(100vh - 150px);
	left: 50px;
	margin-right: 50px;`;

    // Show time
    document.querySelector('[aria-label="Starta uppspelning"]').parentNode.querySelectorAll('span')[0].style = `
	background: RGBA(255, 255, 255, 0.4);
	z-index: 1;
	color: black;`;

    // Control slider with arrowkeys
    let slider = document.querySelector('[aria-label="Vald tid"]');
    function checkKey(e) {
        switch (e.key) {
            case "ArrowLeft":
            case "ArrowRight":
                slider.focus();
                break;
        }
    }
    document.onkeydown = checkKey;
}


// Mini weather radar as favicon
function updateFavicon() {
    let now = new Date();
    let date = now.toISOString().slice(0, 10);
    let min = (now.getUTCMinutes() - 5 + 60) % 60; // Use time 5 minutes ago to make sure image exists on server
    let closest_five = 10*Math.floor(min / 10);
    closest_five += min - closest_five > 4 ? 5 : 0;
    let hours = now.getUTCHours() - (now.getUTCMinutes() < 5 ? 1 : 0); // Roll back an hour if five minutes ago was last hour
    let time = "T" + hours.toString().padStart(2,0) + ":" + closest_five.toString().padStart(2,0) + ":00Z";
    let URI_time = encodeURIComponent(date + time);

    let old_favicon = document.querySelector("link[rel*='icon']");
    let favicon = document.createElement('link');
    favicon.type = 'image/png';
    favicon.rel = 'shortcut icon';

    // BBOX is the bounding box (lower left x, lower left y, upper right x, upper right y)
    // https://epsg.io for coordinates in EPSG format
    // Lower left (Tranås)
    const lower_left = "1661129.480000000,7984306.300000000";
    // Upper right (Skogsby)
    const upper_right = "1815838.090000000,8129842.350000000";
    favicon.href = `https://wpt-wts.smhi.se/tile/?&service=WMS&request=GetMap&layers=baltrad%3Aradarcomp-lightning_sweden_wpt&styles=&format=image%2Fpng&transparent=true&version=1.1.1&time=${URI_time}&dim_reftime=&width=16&height=16&srs=EPSG%3A900913&bbox=${lower_left},${upper_right}`;

    document.head.removeChild(old_favicon);
    document.head.appendChild(favicon);
}

// Auto update
function updateData() {
    let btn = document.querySelector('[aria-label="Hämta senaste datan"]');
    btn.click();

    // Move slider to end to show newest data
    let slider = document.querySelector('[aria-label="Vald tid"]');
    slider.focus();
    slider.value = slider.max;
    slider.dispatchEvent(new KeyboardEvent('keypress',{'key':'End'}));
}
