// ==UserScript==
// @name         Adsbexchange.com - Add mini map
// @namespace    adsbexchange.com
// @version      3
// @description  Add mini map and google maps search
// @match        https://global.adsbexchange.com/VirtualRadar/desktop.html
// @grant        none
// ==/UserScript==

// Prepare Leaflet
var head = document.getElementsByTagName('head')[0];

var leafletCss = document.createElement('link');
leafletCss.setAttribute('rel', 'stylesheet');
leafletCss.setAttribute('href', 'https://unpkg.com/leaflet@1.3.1/dist/leaflet.css');

head.appendChild(leafletCss);

var myStyle = document.createElement('style');
myStyle.innerHTML = `
.icao2 {
  text-decoration: none;
  color: #2e74b5;
}
.icao2:hover {
  text-decoration: underline;
}
`;
document.head.appendChild(myStyle);

function coordinateListener() {
  aircraftList.removeEventListener('click', coordinateListener, false);
  var body = document.querySelector('div.body');
  var attributes = body.querySelectorAll('li');
  var latitude = attributes[7];
  var longitude = attributes[8];

  // Add mini map
  var lat = parseFloat(latitude.lastChild.textContent);
  var lon = parseFloat(longitude.lastChild.textContent);

  var miniMap = document.createElement('div');
  miniMap.style = "width: 100%; height: 300px;";
  miniMap.id = "leaflet-map";
  var detailPane = document.querySelector('#aircraftDetail');
  detailPane.appendChild(miniMap);

  var osmLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { });
  var esriLayer = L.tileLayer(
            'http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
            attribution: '&copy; <a href="http://www.esri.com/">Esri</a>',
            maxZoom: 18
            });
  
  var mymap = L.map('leaflet-map', {
    center: [lat, lon],
    zoom: 5,
    scrollWheelZoom: false,
    layers: [osmLayer]
  });
  
  var mapLayers = {
    'OSM': osmLayer,
    'Satellite': esriLayer
  };  
  L.control.layers(mapLayers).addTo(mymap);  
  var marker = L.marker([lat, lon]).addTo(mymap);    

  var observer = new MutationObserver(mutations => {
    for( var mutation of mutations) {
      for( var added of mutation.addedNodes) {
        latitude = attributes[7];
        longitude = attributes[8];
        searchString = latitude.lastChild.textContent + ' ' + longitude.lastChild.textContent;
        gmapsLink.href = 'https://www.google.com/maps/search/' + searchString;

        // Flight history link
        var icao = document.querySelector('.icao');
        var flightHistoryLink = document.createElement('a');
        flightHistoryLink.href = `https://flight-data.adsbexchange.com/activity?inputSelect=icao&icao=${icao.textContent}`;
        flightHistoryLink.text = icao.textContent;
        flightHistoryLink.classList.add('icao2');
        flightHistoryLink.title = 'Look up flight history';
        icao.textContent = '';
        icao.appendChild(flightHistoryLink);

        // Minimap
        lat = parseFloat(latitude.lastChild.textContent);
        lon = parseFloat(longitude.lastChild.textContent);

        if(!mymap.getBounds().contains([lat, lon])) {
          mymap.setView([lat, lon]); // update view if marker outside bounds
        }
        marker.setLatLng([lat, lon]);
      }
    }
  });
  observer.observe(longitude, {childList: true, subtree: true});
}

var aircraftList = document.getElementById('aircraftList');
aircraftList.addEventListener('click', coordinateListener, false);
