// ==UserScript==
// @name         Feber.se - Tjock link to thumbnails
// @version      0.1
// @description  Link Tjock logo to Tjock thumbnails
// @author       You
// @match        https://feber.se/thumbnails/
// @grant        none
// ==/UserScript==

var tl = document.querySelector('a[href*="/tjock.se/"]');
if (tl) {
    tl.href = "https://tjock.se/thumbnails/";
}