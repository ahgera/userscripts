// ==UserScript==
// @name         Corren - Byt ut Senaste Nytt
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Hämtar listan Senaste Nytt från https://nyponold.corren.se/nyheter/ och stoppar in den i nya Corren
// @match        https://corren.se/nyheter
// @grant        GM.xmlHttpRequest
// @connect      nyponold.corren.se
// ==/UserScript==

let list_items_new = document.evaluate("//div[@class='list-title' and contains(text(), 'Senaste nytt')]/../div[@class='list-items']",
    document, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null).snapshotItem(0);

GM.xmlHttpRequest({
    method: "GET",
    url: "https://nyponold.corren.se/nyheter/",
    onload: function (response) {
        let parser = new DOMParser();
        let doc = parser.parseFromString(response.responseText, "text/html");
        let list_items = xp("//li[contains(text(), 'Senaste nytt')]/../../div/div/div", doc);
        let list_html = list_items.singleNodeValue.innerHTML.replace(/href="/g, 'href="http://nyponold.corren.se');

        list_items_new.innerHTML = list_html;
    }
});

function xp(exp, doc) {
    return doc.evaluate(exp, doc, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null);
}