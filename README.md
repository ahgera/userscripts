# Userscripts for browsers

## Get userscripts
[OpenUserJS](https://openuserjs.org/) - The home of FOSS user scripts.

[Greasy Fork](https://greasyfork.org/en) - An online repository of user scripts.

[Search GitHub](https://gist.github.com/search?l=javascript&q=%22user.js%22)

[TamperMonkey Userscript Sources](https://www.tampermonkey.net/scripts.php)

## Documentation
[Tampermonkey](https://www.tampermonkey.net/documentation.php)

[Mozilla Developer Network](https://developer.mozilla.org/en-US/docs/Web)