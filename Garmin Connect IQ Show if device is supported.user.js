// ==UserScript==
// @name         Garmin Connect IQ Show if device is supported
// @namespace    http://tampermonkey.net/
// @version      0.5
// @description  Quickly shows if a device is supported
// @author       You
// @match        https://apps.garmin.com/*
// @icon         https://www.google.com/s2/favicons?domain=garmin.com
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    let devices = ["vívoactive® 3\n"];

    let comp_link = document.querySelector("#nav-tab-compatibledevices > a");
    if (!comp_link) return;

    let devicelist = document.querySelector("#compatible-devices");

    let compatible = [];
    for (let d of devices) {
        if (devicelist.textContent.indexOf(d) > 0) {
            compatible.push(d);
        }
    }

    if (compatible.length > 0) {
        if (compatible.length == devices.length)
            comp_link.text += " ✅";
        else
            comp_link.text += " ✔";

        comp_link.title = compatible.join('\n').trim();
    }
    else {
        comp_link.text += " ❌";
    }
})();