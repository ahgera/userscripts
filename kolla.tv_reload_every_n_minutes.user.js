// ==UserScript==
// @name         Kolla.tv - Reload every n minutes
// @namespace    none
// @version      0.1
// @description  Reload every n minute
// @author       You
// @match        http://kolla.tv/*
// @match        https://kolla.tv/*
// @grant        none
// ==/UserScript==

setTimeout(function() { location.reload(true); }, 5*60*1000); // Every five minutes