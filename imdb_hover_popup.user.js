// ==UserScript==
// @name         IMDb Hover popup
// @version      0.6
// @description  Show a preview popup when hovering over a link
// @author       You
// @match        https://www.imdb.com/name/*
// @grant        none
// ==/UserScript==

// Popup element
let popup = document.createElement('div');
popup.innerHTML =
    `<h2 id='popup_title'>Movie title</h2>
    <div style='display: flex;'>
      <div style='width: 35%; margin: 5px;'>
        <img id='popup_img' style='max-width:100%; height: auto;' src='https://m.media-amazon.com/images/M/MV5BMjM1NTc0NzE4OF5BMl5BanBnXkFtZTgwNDkyNjQ1NTE@._V1_UX182_CR0,0,182,268_AL_.jpg'>
      </div>
      <div style='width: 65%; margin: 5px;'>
        <div id='popup_genre'><strong>Genre: </strong><span></span></div>
        <div id='popup_director'><strong>Director: </strong><span></span></div>
        <div id='popup_stars'><strong>Stars: </strong><span></span></div>
        <div id='popup_description'>Description</div>
      </div>
    </div>`;
popup.id = 'popup';
popup.className = 'pophidden';
document.getElementsByTagName('body')[0].appendChild(popup);

// Move popup with mouse pointer
document.body.addEventListener("mousemove", (e) => {
    let p = document.getElementById('popup');
    p.style.setProperty("--mouse-x", e.clientX + 'px');
    p.style.setProperty("--mouse-y", e.clientY + 'px');
});

// Add mouse over function to all movie links
let filmography = document.querySelector('#filmography');
let movieLinks = filmography.getElementsByTagName('a');
for (let ml of movieLinks) {
    ml.addEventListener("mouseover", fetchInfo);
    ml.addEventListener("mouseout", hidePopup);
}

// Description cache
let descriptions = {};

async function fetchInfo(e) {
    function joinArray(arr) {
        return Array.isArray(arr) ? arr.join(', ') : arr;
    }
    function joinPersonArray(persons) {
        if (!persons) return 'None specified';
        if (Array.isArray(persons))
        {
            let str = '';
            for (let i = 0; i < persons.length; i++) {
                str += persons[i]['name'];
                if (i < persons.length - 1)
                    str += ', ';
            }
            return str;
        }
        else {
            return persons['name'];
        }
    }

    e.target.dataset.showPopup = true;

    let p = document.getElementById('popup');
    let title = document.querySelector('#popup_title');
    let img = document.getElementById('popup_img');
    let genre = document.querySelector('#popup_genre > span');
    let director = document.querySelector('#popup_director > span');
    let stars = document.querySelector('#popup_stars > span');
    let description = document.querySelector('#popup_description');

    let movieId = e.target.href.match(/tt\d+/)[0];
    let desc = {};
    if (descriptions[movieId]) {
        desc = descriptions[movieId];
        p.classList.add('cached');
    }
    else {
        desc = await fetchDescription(e.target.href);
        descriptions[movieId] = desc;
    }

    title.textContent = desc['name'];
    img.src = desc['image'];
    genre.textContent = joinArray(desc['genre']);
    director.textContent = joinPersonArray(desc['director']);
    stars.textContent = joinPersonArray(desc['actor']);
    description.textContent = joinArray(desc['description']);

    // Get new size 
    let rect = p.getBoundingClientRect();
    p.style.setProperty("--width", rect.width + 'px');
    p.style.setProperty("--height", rect.height + 'px');

    // Check if the popup for this link should still be shown
    if (e.target.dataset.showPopup === 'true')
    {
        p.classList.remove('pophidden');
        p.classList.add('popvisible');
    }
}

async function fetchDescription(url) {
    let response = await fetch(url, { credentials: 'omit' });
    let parser = new DOMParser();
    let html = await response.text();
    let doc = parser.parseFromString(html, "text/html");
    let desc = JSON.parse(doc.querySelector('script[type="application/ld+json"]').textContent);
    return desc;
}

function hidePopup(e) {
    let p = document.getElementById('popup'); 
    p.className = 'pophidden';
    e.target.dataset.showPopup = false;
}

// CSS for popup
addStyle(`
    #popup {
        --mouse-x: 0px;
        --mouse-y: 0px;
        --width: 100px;
        --height: 100px;
        font-size: 15px;
        line-height: 18px;
        background: #eee;
        max-width: 450px;
        border-radius: 10px; 
        border: 4px solid #777;
        padding: 5px;
        z-index: 1;
        box-shadow: 1px 1px 4px #555A;
        position: fixed;
        left: calc(var(--mouse-x) - var(--width) / 2);
        top: calc(var(--mouse-y) - var(--height) - 17px);
        transition-property: visibility, opacity;
        transition-timing-function: ease-in-out;
    }
    #popup.popvisible {
        visibility: visible;
        opacity: 1;
        transition-duration: 0s, 0.3s;        
        transition-delay: 0.1s;
    }
    #popup.cached {       
        transition-delay: 0.6s !important;
    }
    #popup.pophidden {
        visibility: hidden;
        opacity: 0;
        transition-duration: 0.2s;        
        transition-delay: 0.1s, 0s;
    }
    #popup > h2 {
        font-size: 17px;
        font-weight: bold;
        text-align: center;
    }
    #popup > img {
        display: block;
        margin-left: auto;
        margin-right: auto;
        border-radius: 5px;
        max-width: 200px;
        max-height: 250px;
    }
    #popup_description {
        padding: 5px 0 0 0;
    }
    #popup::after {
        content: " ";
        position: absolute;
        top: 100%;
        left: 50%;
        margin-left: -10px;
        border-width: 10px;
        border-style: solid;
        border-color: #777 transparent transparent transparent;
    }
`);

function addStyle(stylestr) {
    let style_el = document.createElement('style');
    style_el.innerHTML = stylestr;
    document.getElementsByTagName('head')[0].appendChild(style_el);
}

function xp(exp, doc = document) {
    return doc.evaluate(exp, doc, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
}
