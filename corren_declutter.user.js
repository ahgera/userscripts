// ==UserScript==
// @name         Corren Declutter
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Lower opacity of non Linköping related article blurbs
// @author       You
// @match        https://www.corren.se/nyheter/
// @grant        none
// ==/UserScript==

let whitelist = ["Linköping", "Brott", "Nyheter", "Väder", 
                 "Affärsliv", "Östergötland", "Musik"];

let articles = document.querySelectorAll("article.blurb");

for (let a of articles) {
    let category = a.querySelector("span.blurb-cat");
    if (category && !whitelist.includes(category.textContent)) {
        a.style = "opacity: 0.25;"
    }
}
