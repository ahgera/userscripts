// ==UserScript==
// @name         Addnature.se - Fix image zoom
// @namespace    http://tampermonkey.net/
// @version      1
// @description  Remove class name from zoomed in image to show image instead of spinner
// @author       Anders
// @match        https://www.addnature.com/*
// @grant        none
// ==/UserScript==

(function() {
    let elms = document.querySelectorAll('div.pdp-gallery_zoom');
    elms.forEach(el => {
        el.classList.remove('pdp-gallery_zoom');
    });
})();